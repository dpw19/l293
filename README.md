# Diver para dos motores de corriente directa basado en el CI L293D

## Introducción 
En el mundo de los microcontroladores es frecuente controlar
cargas, en este caso la carga son dos motores, cuyo consumo
de corriente y características de voltaje son mucho mayores 
que los manejados y proporcionados por un microcontrolador.

Para lograr esto necesitamos un **Driver** o **Interface**
que reciba señales de control desde un microcontrolador
y manipule con ellas grandes cantidades de corriente.

## Carpetas
En la carpeta
* **Kicad**: Contiene el proyecto del PCB realizado con KiCAD.
* **Gráficos**: Contiene los PDF de las pistas, barniz y letreros.

## Lista de componentes
Se encuentran en la carpeta KiCAD bajo el nombre [l293_bom.html](kicad/l293_bom.html)


| Ref |	Qnty |Value | Description|
|-----|------|------|------------|
|C1   |	1    |	10uF| Capacitor electrollitico |
|C2, C3|2    | 	22nF| Capacitor cerámico |	
|J1   |	1    |Vcc   |	Header |	
|J2   |	1    |Vmotor|	Header |	
|J3   |	1    |Ctrl M2|	Header |		
|J4   |	1    |CTRL M1|	Header |			
|J5   |	1    |M1    |	Header |	
|J6   | 1    |M2    | Header |	
|U1   |	1    |L293  |	Driver Motor:L293 DIP-16 |	


## Diagrama de conexiones

La figura muestra como realizar las conexiones

![Conexiones del circuito](graficos/conexiones.png)

### M1, M2
Son los pines a los que va conectado cada motor,
la flecha indica el pin sugerido como positivo.

### CM1, CM2
Son los pines a los que va conectado el control de cada motor,
Suelen venir de un microcontrolador como Arduino.

### Vmotor
Es el voltaje de polarización del motor puede ir de 0 a 12V.  
Depende del motor que se utilice.  
La flecha indica el polo positivo.  

### Vcc
Es el voltaje de polarización del circuito L293D, debee ser
de 5V.  
Usualmente se obtiene de la placa de desarrollo.  
La flecha indica el polo positivo.  

### Nota
Las conexiones a tierra GND tanto del L293D como de las
fuentes de Vcc y Vmotor están conectadas entre si en el PCB.


